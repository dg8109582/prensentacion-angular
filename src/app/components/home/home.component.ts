import { Component, OnInit } from '@angular/core';
import { RyMService } from 'src/app/services/RickandMorty/ry-m.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public personajes : any [] = [];

  constructor(private _rym : RyMService) { }

  ngOnInit(): void {
    this.getPersonajes();
  }

  getPersonajes() {

    

    this._rym.getPersonajes()
    .subscribe((data:any) => {
      console.log(data);

    
      
    });

  }

}
