import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class RyMService {

  public URL = 'https://rickandmortyapi.com/api';

  constructor( private _http : HttpClient) { }

  getPersonajes(){
    const url = `${this.URL}/character`;
    return this._http.get(url);
  }
}
