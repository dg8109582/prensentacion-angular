import { TestBed } from '@angular/core/testing';

import { RyMService } from './ry-m.service';

describe('RyMService', () => {
  let service: RyMService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RyMService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
